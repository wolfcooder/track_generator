import { Injectable } from '@angular/core';
import {TimeInterval} from '../dto/time-interval-dto';

@Injectable({
  providedIn: 'root'
})
export class RandomService {

  constructor() { }

  fromTimeInterval(interval: TimeInterval) {
    return Math.round(
      Math.random() * (interval.toMin - interval.fromMin) + interval.fromMin
    );
  }
}
