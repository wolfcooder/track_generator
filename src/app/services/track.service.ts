import { Injectable } from '@angular/core';
import {Route, RouteSegment, RouteService} from './route.service';
import {TrackRequestDto} from '../dto/track-request-dto';
import {TrackPackageDto} from '../dto/track-package-dto';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import * as moment from 'moment';
import {GeoPointDto} from '../dto/geo-point-dto';
import {RandomService} from './random.service';

@Injectable({
  providedIn: 'root'
})
export class TrackService {

  constructor(private route: RouteService, private random: RandomService) { }

  public get(request: TrackRequestDto): Observable<TrackPackageDto[]> {
    return this.route.get(request.routePoints.map(p => p.point)).pipe(
      map(route => this.build(request, route))
    );
  }

  private build(request: TrackRequestDto, route: Route): TrackPackageDto[] {

    console.log('Track building... ');
    console.log('Request:: ', request);
    console.log('Route:: ', route);

    let totalDurationMS = 0;

    let totalWaitingMS = 0;

    let track: TrackPackageDto[] = [];

    route.segments.forEach(segment => {
      const waitingMS = request.routePoints.find(p => p.point.lat === segment.from.lat && p.point.lon === segment.from.lon).waitingTimeMin * 60 * 1000;
      const beginTimeMS = totalDurationMS + totalWaitingMS + moment( request.beginDatetime ).unix() * 1000
      track = track.concat(this.buildSegment(segment, beginTimeMS, waitingMS, request.maxSpeed));
      totalDurationMS += segment.durationMs;
      totalWaitingMS += waitingMS;
    });

    return track;
  }

  private buildSegment(segment: RouteSegment, beginTimeMS: number, waitingMS: number, maxSpeed: number): TrackPackageDto[] {

    let track: TrackPackageDto[] = [];

    for (let i = waitingMS; i >= 0; i -= 30 * 60 * 1000) {
      track.push ({
        direction: this.random.fromTimeInterval({fromMin: 0, toMin: 359}),
        ignition: 0,
        point: segment.from,
        speed: 0,
        time: beginTimeMS + (waitingMS - i)
      });
    }

    track = track.concat(segment.polyline.map((point, index, src) => {
      return {
        direction: this.getAngleByPoints(segment.polyline[index + 1], point),
        ignition: (point === segment.from) || (point === segment.to) ? 0 : 1,
        point: point,
        speed: (point === segment.from) || (point === segment.to)
          ? 0
          : (index < 2 || index > src.length - 2)                           // Первые два пакета после старта скорость не более 10
            ? (this.random.fromTimeInterval({fromMin: 0, toMin: 10}))
            : (this.random.fromTimeInterval({fromMin: 0, toMin: maxSpeed})),

        time: ( ( index / segment.polyline.length ) * segment.durationMs ) + beginTimeMS + waitingMS
      };
    }).map((trackPackage, index, src) => {
      if (src[index + 1] && (Math.abs(trackPackage.direction - src[index + 1].direction) > 45)) {
        trackPackage.speed = this.random.fromTimeInterval({fromMin: 0, toMin: 10});     // Если угол поворота больше 45 скорость не более 10
      }
      return trackPackage;
    }));

    console.log(segment.polyline);
    return track;
  }

  private getAngleByPoints(point1: GeoPointDto, point2: GeoPointDto) {
    let angle = ( point1 && point2 ) ? Math.atan2(
      point2.lon - point1.lon,
      point2.lat - point1.lat
    ) * 180 / Math.PI - 180 : 0;
    if (angle < 0) {
      angle += 360;
    }
    return Math.round(angle);
  }
}
