import { Injectable } from '@angular/core';
import {TrackPackageDto} from '../dto/track-package-dto';
import * as moment from 'moment';
import { saveAs } from 'file-saver';

@Injectable({
  providedIn: 'root'
})
export class WialonService {

  constructor() { }

  public encode(track: TrackPackageDto[]): string {
    let output = '';
    track.forEach(trackPackage => output += this.encodePackage(trackPackage) + '\r\n');
    return output;
  }

  public download(track: TrackPackageDto[], filename: string) {
    saveAs(new Blob([this.encode(track)], {type: 'text/wln'}), `${filename}.wln`);
  }

  private encodePackage(trackPackage: TrackPackageDto): string {
    return 'REG;'
      + moment(trackPackage.time).unix() + ';'
      + trackPackage.point.lon + ';'
      + trackPackage.point.lat + ';'
      + trackPackage.speed + ';'
      + trackPackage.direction + ';'
      + 'ALT:0.0,'
      + 'accuracy:0.0,'
      + 'din1:' + trackPackage.ignition + '.0,'
      + 'rel1:0.0;'
      + 'SATS:255;;;;';
  }
}
