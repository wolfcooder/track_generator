import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import * as polyline from 'google-polyline';
import * as moment from 'moment';
import {Observable} from 'rxjs';
import {RouteResponseDto} from '../dto/route-response-dto';
import {GeoPointDto} from '../dto/geo-point-dto';
import {map} from 'rxjs/operators';


export interface Route {
  all?: RouteSegment;
  segments?: RouteSegment[];
}

export interface RouteSegment {
  from?: GeoPointDto;
  to?: GeoPointDto;
  durationMs?: number;
  distanceKm?: number;
  polyline?: GeoPointDto[];
}
@Injectable({
  providedIn: 'root'
})
export class RouteService {

  private url = 'http://geo.smarttaxi.ru/1.x/route?taxiServiceId=taxity';

  constructor(private http: HttpClient) {
  }

  get(points: GeoPointDto[]): Observable<Route> {
    return this.http.post(this.url, points).pipe(
      map(res => {
        const result: RouteResponseDto = {};
        Object.assign<RouteResponseDto, any>(result, res);
        return result;
      }),
      map(routeResponse => {
        const allPolyline = this.decodePolyline(routeResponse.overviewPolyline);
        return {
          all: {
            from: routeResponse.from,
            to: routeResponse.to,
            polyline: allPolyline,
            distanceKm: routeResponse.distance.kilometers,
            durationMs: this.calcDurationMs(routeResponse.duration)
          },
          segments: routeResponse.segments.map(segment => {
            return {
              from: segment.from,
              to: segment.to,
              distanceKm: segment.distance.kilometers,
              durationMs: this.calcDurationMs(segment.duration),
              polyline: this.segmentPolyline(allPolyline, segment)
            };
          })
        };
      })
    );
  }

  private calcDurationMs(duration: string): number {
    return moment('01.01.1970 ' + duration).utc(true).unix() * 1000;
  }

  private segmentPolyline(src: GeoPointDto[], segment: RouteSegment): GeoPointDto[] {
    const result: GeoPointDto[] = [];
    let active = false;

    src.forEach(point => {
      if (Math.abs(point.lat - segment.from.lat) < 0.0003 && Math.abs(point.lon - segment.from.lon) < 0.0003) {
        active = true;
      }
      if (active) {
        result.push(point);
      }
      if (Math.abs(point.lat - segment.to.lat) < 0.0003 && Math.abs(point.lon - segment.to.lon) < 0.0003 ) {
        active = false;
      }

    });

    return result;

  }

  private decodePolyline(src: string): GeoPointDto[] {
    return polyline.decode(src).map(
      (point): GeoPointDto => {
        return {lat: Math.round(point[0] * 10000) / 10000, lon: Math.round(point[1] * 10000) / 10000};
      }
    );
  }
}
