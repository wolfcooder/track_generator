import {GeoPointDto} from './geo-point-dto';

export interface RouteResponseDto {
  from?: GeoPointDto;
  to?: GeoPointDto;
  fromDescription?: string;
  toDescription?: string;
  duration?: string;
  distance?: DistanceDto;
  segments?: RouteSegmentDto[];
  error?: 0 | 1;
  overviewPolyline?: string;
}

export interface DistanceDto {
  kilometers: number;
  meters: number;
}

export interface RouteSegmentDto {
  from?: GeoPointDto;
  to?: GeoPointDto;
  fromDescription: GeoPointDto;
  toDescription: GeoPointDto;
  duration: string;
  distance: DistanceDto;
}
