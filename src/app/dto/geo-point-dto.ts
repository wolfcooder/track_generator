export interface GeoPointDto {
  lat: number;
  lon: number;
}
