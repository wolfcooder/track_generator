import {GeoPointDto} from './geo-point-dto';
import {TimeInterval} from './time-interval-dto';

export interface TrackPoint {
  point: GeoPointDto;
  waitingTimeMin: number;
}

export interface TrackRequestDto {
  carNumber?: string;
  beginDatetime?: Date;
  endDatetime?: Date;
  beginWaiting?: TimeInterval;
  intermediateWaiting?: TimeInterval;
  routePoints?: TrackPoint[];
  maxSpeed?: number;
}
