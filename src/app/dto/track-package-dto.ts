import {GeoPointDto} from './geo-point-dto';

export interface TrackPackageDto {
  time?: number;
  point?: GeoPointDto;
  speed?: number;
  direction?: number;
  ignition?: number;
}
