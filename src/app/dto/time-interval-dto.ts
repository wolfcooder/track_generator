export interface TimeInterval {
  fromMin: number;
  toMin: number;
}
