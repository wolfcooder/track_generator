import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AngularOpenlayersModule } from 'ngx-openlayers';

import { AppComponent } from './app.component';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import { GeoPointInputComponent } from './views/common/geo-point-input/geo-point-input.component';
import { TextInputComponent } from './views/common/text-input/text-input.component';
import { DatetimeInputComponent } from './views/common/datetime-input/datetime-input.component';
import {OsmViewComponent} from './views/osm-view/osm-view.component';
import { RangeInputComponent } from './views/common/range-input/range-input.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NumberInputComponent } from './views/common/number-input/number-input.component';

@NgModule({
  declarations: [
    AppComponent,
    GeoPointInputComponent,
    TextInputComponent,
    DatetimeInputComponent,
    OsmViewComponent,
    RangeInputComponent,
    NumberInputComponent
  ],
  imports: [
    BrowserModule,
    CommonModule,
    FormsModule,
    HttpClientModule,
    AngularOpenlayersModule,
    BrowserAnimationsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
