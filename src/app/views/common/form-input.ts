export interface FormInput<T> {
  data: T;
  title: string;
}
