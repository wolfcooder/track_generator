import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {GeoPointDto} from '../../../dto/geo-point-dto';
import {FormInput} from '../form-input';
import {TrackPoint} from '../../../dto/track-request-dto';

@Component({
  selector: 'app-geo-point-input',
  templateUrl: './geo-point-input.component.html',
  styleUrls: ['./geo-point-input.component.css']
})
export class GeoPointInputComponent implements OnInit, FormInput<TrackPoint> {

  @Input() data: TrackPoint;
  @Input() title: string;

  @Output() remove = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

}
