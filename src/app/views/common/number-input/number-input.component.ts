import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormInput} from '../form-input';

@Component({
  selector: 'app-number-input',
  templateUrl: './number-input.component.html',
  styleUrls: ['./number-input.component.css']
})
export class NumberInputComponent implements OnInit, FormInput<number> {

  @Input() title: string;
  @Input() data: number;
  @Output() dataChanged = new EventEmitter<number>();

  constructor() { }

  ngOnInit() {
  }

}
