import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormInput} from '../form-input';

@Component({
  selector: 'app-text-input',
  templateUrl: './text-input.component.html',
  styleUrls: ['./text-input.component.css']
})
export class TextInputComponent implements OnInit, FormInput<string> {

  @Input() title: string;
  @Input() data: string;
  @Output() dataChanged = new EventEmitter<string>();

  constructor() { }

  ngOnInit(): void {
  }

}
