import {Component, Input, OnInit} from '@angular/core';
import {FormInput} from '../form-input';
import {TimeInterval} from '../../../dto/time-interval-dto';

@Component({
  selector: 'app-range-input',
  templateUrl: './range-input.component.html',
  styleUrls: ['./range-input.component.css']
})
export class RangeInputComponent implements OnInit, FormInput<TimeInterval> {

  @Input() data: TimeInterval;
  @Input() title: string;

  constructor() { }

  ngOnInit() {
  }


}
