import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormInput} from '../form-input';
import * as moment from 'moment';

@Component({
  selector: 'app-datetime-input',
  templateUrl: './datetime-input.component.html',
  styleUrls: ['./datetime-input.component.css']
})
export class DatetimeInputComponent implements OnInit, FormInput<Date> {

  @Input() data: Date;
  @Input() title: string;

  @Output() dateChange = new EventEmitter<Date>()

  constructor() { }

  get inputDate(): string {
    return moment(this.data).format('yyyy-MM-DDThh:mm');
  }

  set inputDate(date: string) {
    this.data = new Date(date);
    this.dateChange.emit(this.data);
  }

  ngOnInit(): void {
  }

}
