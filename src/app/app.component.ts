import {Component} from '@angular/core';
import {GeoPointDto} from './dto/geo-point-dto';
import {TrackRequestDto} from './dto/track-request-dto';
import {TrackService} from './services/track.service';
import {WialonService} from './services/wialon.service';
import {transliterate as tr, slugify} from 'transliteration';
import {RouteService} from './services/route.service';
import {RandomService} from './services/random.service';
import {Time} from '@angular/common';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'track-generator';

  request: TrackRequestDto = {
    carNumber: 'Е 777 КХ 199',
    beginDatetime: new Date(),
    intermediateWaiting: {fromMin: 10, toMin: 20},
    routePoints: [],
    beginWaiting: {fromMin: 10, toMin: 20},
    maxSpeed: 60
  };

  points: GeoPointDto[] = [];
  polyline: GeoPointDto[] = [];
  summaryDurationMs = 0;
  overtimeLimit = new Date(12 * 60 * 60 * 1000);

  constructor(private trackService: TrackService,
              private wialonService: WialonService,
              private routeService: RouteService,
              private random: RandomService) {
  }

  get summaryDuration() {
    return new Date(this.summaryDurationMs + this.summaryWaitingMs);
  }

  get summaryWaitingMs() {
    let result = 0;
    this.request.routePoints.forEach(p => result += p.waitingTimeMin * 1000 * 60);
    return result;
  }

  getTrack() {
    this.trackService.get(this.request).subscribe(
      track => {
        console.log('Track:: ', track);
        this.polyline = track.map(p => p.point);
        this.wialonService.download(track, tr(this.request.carNumber));
      });
  }

  addPoint(point: GeoPointDto) {
    this.request.routePoints.push(
      {
        point: {lat: (Math.round(point.lat * 100000000) / 100000000), lon: Math.round(point.lon * 100000000) / 100000000},
        waitingTimeMin: ( this.points.length > 1 )
          ? this.random.fromTimeInterval(this.request.intermediateWaiting)
          : this.random.fromTimeInterval(this.request.beginWaiting)
      }
    );
    this.updatePloyline();
  }

  removePoint(point: GeoPointDto) {
    this.request.routePoints = this.request.routePoints.filter(p => p.point !== point);
    this.points = this.request.routePoints.map(p => p.point);
    this.updatePloyline();
  }

  get valid(): boolean {
    return this.request.routePoints.length >= 2;
  }

  get overtime(): boolean {
    return this.summaryDuration > this.overtimeLimit;
  }

  updatePloyline() {
    if (this.points.length >= 2) {
      this.routeService.get(this.points).subscribe(
        route => {
          this.polyline = route.all.polyline;
          this.summaryDurationMs = 0;
          route.segments.forEach(s => this.summaryDurationMs += ( s.durationMs ));
        }
      );
    } else {
      this.polyline = [];
    }
  }
}
